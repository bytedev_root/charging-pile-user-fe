# vue语法微信小程序 css实现刻度尺

最近需要实现一个高度定制的刻度尺，但是网上现成的方案却是极少，找到过是原生微信开发的但不是 vue开发小程序。

本插件使用了vue语法和`scroll-view`标签以及其属性`scroll-left`。若想在除微信小程序其他小程序或项目中使用。只要是能满足以上两个条件均可使用该插件。

目前发现**支付宝小程序**、**QQ小程序**、**字节跳动(今日头条)小程序 **和 **百度小程序** 的开发文档中均有`scroll-view`标签和其属性` scroll-left`。因此这些小程序均可使用，可能在不同小程序中表现会有所差异，本人目前只是在微信小程序中使用。

### 1.效果图
![vue-scale](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-4d30057b-f677-41d0-ae58-b333da00d9aa/c4073e02-1175-4e6f-b121-26ab76e89976.jpg)

### 2.使用
1. 将其当成自定义组件引入到项目中

```
import vueScale from '@/components/sapling-vue-scale/sapling-vue-scale.vue';
```

2. 页面调用

```html
<div>
      <view class='scale-title'>横向没有小数点</view>
      <view class='scale-value'>{{horizontaNoPointVal}}</view>
      <vue-scale :min="10"
                 :max="50"
                 :int="true"
                 :single="10"
                 :h="80"
                 :active="36"
                 :styles="styles"
                 @value="horizontaNoPointMethods"/>
    </div>
    <div>
      <view class='scale-title'>横向有小数点</view>
      <view class='scale-value'>{{horizontaPointVal}}</view>
      <vue-scale :min="10"
                 :max="50"
                 :int="false"
                 :single="10"
                 :h="80"
                 :active="36.1"
                 :styles="styles"
                 @value="horizontaPointMethods"/>
    </div>
```
```js
components: {
    vueScale,
},
data() {
   return {
     weight: 11,
     height: 180,
     styles: {
        line: '#dbdbdb',
        bginner: '#fbfbfb',
        bgoutside: '#ffffff',
        font: '#404040',
        fontColor: '#404040',
        fontSize: 16,
     },
     horizontaPointVal: '',
     horizontaNoPointVal: '',
  };
},
methods: {
    // 横向滚动有小数点
    horizontaPointMethods(msg) {
      this.horizontaPointVal = msg;
    },
    // 横向滚动没有小数点
    horizontaNoPointMethods(msg) {
      this.horizontaNoPointVal = msg;
    },
},
```
### 3.参数说明

|参数名|默认值|说明|
|:-:|:-----:| :----: |
|min| 0|最小值|
|max| 100|最大值|
| int | true| 是否开启整数模式|
|direction|'vertical'|'vertical' 纵向，'horizontal' 横向|
|single|10|单个格子的实际长度（单位px）一般不建议修改|
|h| 80|自定义高度，当direction='vertical'时未宽度|
|active|（min+max）/2|自定义选中位置 ,有效值(min-max)、min、max、center|
|styles|{...}|自定义卡尺样式|
|scroll|true|true代表不禁止滑动|

style选项

|参数名|默认值|说明|
|:-:|:-----:| :----: |
|line|#dbdbdb|刻度颜色|
|bginner|#fbfbfb|前景色颜色|
| bgoutside |#dbdbdb| 背景色颜色|
|lineSelect|#6643e7|选中线颜色|
|fontColo| #404040|刻度数字颜色|
|fontSiz|16|字体大小|

有不对或者什么问题，可以直接留言。
