import request from '../utils/request'
import request2 from '../utils/request2'

//
export function loginForWechat(data){//登录-微信授权
    return request.request('/ts/api/tenants/login/loginForWechat','post',data,false,'notoken')
}
export function getUserPhoneForWechat(data){//微信-获取用户手机号码
    return request.request('/ts/api/tenants/login/getUserPhoneForWechat','post',data,false,'notoken')
}


export function sendMsmForBindingPhone(phoneNum){//绑定手机号码-发送短信
    return request.request('/ts/api/tenants/userInfo/sendMsmForBindingPhone?phoneNum='+phoneNum,'get',null,false)
}

export function bindingPhone(data){//微信-绑定手机
    return request.request('/ts/api/tenants/userInfo/bindingPhone','post',data,false)
}
// export function wxJsPay(data){//支付-微信JS支付
//   return request2.request('/pay/api/unionPay/wxJsPay','post',data,false)
// }
export function wxJsPay(data){//支付-微信JS支付
  return request.request('/ts/api/unionPay/wxJsPay','post',data,false)
}
