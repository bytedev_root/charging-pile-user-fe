export function wx_login() {
	return new Promise(function(reslove, reject) {
		wx.login({
			success: (res) => {
				console.log(res);
				reslove(res.code);
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}

export function wx_getUserProfile() {
	return new Promise((resolve, reject) => {
		wx.getUserProfile({
			desc: '用于完善用户信息',
			success: res => {
				console.log(res);
				resolve(res)
			},
			fail: (err) => {
				reject(err)
			}
		})
	})
}
