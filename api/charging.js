import request from '../utils/request'
// const port = ':8088'

//查询-充电站-列表
export function FindChargingPileList(data){
    return request.request('/ts/api/tenants/CharginPile/FindChargingPileList','post',data,false)
}
//充电站详情
export function findCharginByCode(data){
    return request.request('/ts/api/tenants/CharginPile/findCharginByCode','get',data,false,'formData')
}

//查询-用户钱包
export function findTenantsPay(data){
    return request.request('/ts/api/tenants/pay/findTenantsPay','post',data,false,'formData')
}

//充值-口袋币
export function topUpCoins(bigDecimal){
    return request.request('/ts/api/tenants/pay/topUpCoins?bigDecimal='+bigDecimal,'get',null,false,'formData')
}

//操作-下单充电
export function startCharging(data){
    return request.request('/ts/api/tenants/CharginPile/startCharging','post',data,false)
}

//查询-充电站订单列表
export function findCharginOrderList(data){
    return request.request('/ts/api/tenants/CharginPile/findCharginOrderList','post',data,false)
}

//查询-充电站订单列表
export function findCharginOrderById(orderId){
    return request.request('/ts/api/tenants/CharginPile/findCharginOrderById?orderId='+orderId,'get',null,false)
}

//操作-手动结束订单
export function stopOrder(orderId){
    return request.request('/ts/api/tenants/CharginPile/stopOrder?orderId='+orderId,'get',null,false)
}

//充电站-首页
export function indexForCharginPile(data){
    return request.request('/ts/api/tenants/CharginPile/indexForCharginPile','post',data,false)
}

//生成口袋币购买订单
export function genPdCoinTrade(data){
    return request.request('/ts/api/wxpay/genPdCoinTrade','post',data,false)
}



