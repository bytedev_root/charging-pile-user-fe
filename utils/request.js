import __config from '../config/env.js'

const request = (url, method, data, showLoading, contentType) => {
  let _url = __config.basePath + url
  return new Promise((resolve, reject) => {
      if (showLoading) {
        uni.showLoading({
          title: '加载中',
        })
      }
      var head = {
        "Accept": "application/json",
		  // "token": uni.getStorageSync('token') ? uni.getStorageSync('token') : '' ,
      }
	  
	  if(uni.getStorageSync('token')){
		  head["token"] = uni.getStorageSync('token') 
	  }
      if (contentType == 'formData') {
        head["Content-Type"] = "application/x-www-form-urlencoded"
      }

      uni.request({
        url: _url,
        method: method,
        data: data,
        header: head,
       success(res) {
		   

         if (res.data.code == 200) {
           resolve(res.data)
         } else if (res.data.code == 500) {
           uni.showModal({
             title: '提示',
             content: res.data.message == '' ? '系统繁忙' : res.data.message ,
       	
           })
       	
           reject(res.data)
       
         } 
		else if (res.data.code == 1001) {
		  uni.showToast({
		    title: "请登录",
		    icon: "error",
		    duration: 2000,
					 success:function(){
					 	
					 }
		  })
		  setTimeout(function () {
		    uni.reLaunch({
		      url: "/pages/login/loginwechat"
		    })
		  }, 1000)
		  reject()
		  return false
		} else if (res.data.message == 'No message available') {
		   uni.showToast({
		     title: "请登录",
		     icon: "error",
		     duration: 2000,
		 			 success:function(){
		 			 	
		 			 }
		   })
		   setTimeout(function () {
		     uni.reLaunch({
		       url: "/pages/login/loginwechat"
		     })
		   }, 1000)
		   reject()
		   return false
		 }else if (res.data.code == 3001) {
           
           resolve(res.data)
           return false
         } else {
           uni.showModal({
             title: '提示',
             content: res.data.message,
           })
       
           reject()
         }
       },
        fail(error) {
          console.log(error)
          // uni.showModal({
          //   title: '提示',
          //   content: '接口请求出错：' + error.errMsg,
          //   success(res) {

          //   }
          // })
          reject(error)
        },
        complete(res) {
          uni.hideLoading()
        }
      })
    }
  )
}
module.exports = {
  request
}
